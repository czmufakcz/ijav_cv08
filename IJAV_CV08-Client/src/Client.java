import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client {
    
    private static final String HOST = "localhost";
    private static final int PORT = 8080;
    
    private Socket socket;
    private Scanner scanner;
    
    public Client() {
        try {
            this.socket = new Socket(HOST,PORT);
            System.out.println("Client is ready.");
            System.out.println("Write message:");
            this.scanner = new Scanner(System.in);
            
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            
            while(true) {
                String text = scanner.nextLine();
                writer.write(text + "\r\n");
                writer.flush();
                System.out.println("Message: " + text);
            }
            
            
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static void main(String[] args) {
        Client client = new Client();
    }
    
}
