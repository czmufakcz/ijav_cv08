import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private static final int PORT = 8080;
    private ServerSocket serverSocket;

    public Server() {
        try {
            this.serverSocket = new ServerSocket(PORT);
            System.out.println("Server started");

            while (true) {
                Socket clientSocket = this.serverSocket.accept();
                System.out.println(clientSocket.getRemoteSocketAddress().toString());

                Thread clientThread = new Thread(() -> {
                    System.out.println("Client connected. ");
                    BufferedReader reader;
                    try {
                        reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                        while (true) {
                            String text;
                            text = reader.readLine();
                            System.out.println("New message");
                            System.out.println(text);
                        }

                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                });
                clientThread.start();
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }
    }

    public static void main(String[] args) {
        Server server = new Server();
    }

}
