import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;

public class Main {
    
    
    public static void main(String[] args) throws IOException {
        // Task 1
        InetAddress address = InetAddress.getByName("www.google.com");
        System.out.println(address.getHostAddress());
        
        // Task 2
        URL url = new URL("https://www.upce.cz");
        URLConnection con = url.openConnection();
        
        System.out.println(con.getContentType());
        
        
        InputStreamReader inputStream = new InputStreamReader(con.getInputStream());
        BufferedReader buffer = new BufferedReader(inputStream);
        String line;
        
        while((line= buffer.readLine()) != null) {
            System.out.println(line);
        }
        
    }
}
