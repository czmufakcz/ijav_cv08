# Zadání úkolů

1. Pomocí třídy InetAddress zjistěte IP adresu www.google.com a ověřte s cmd.
2. Vypište obsah stránky https://www.upce.cz  pomocí třídy URL a URLConnection.
    + Zjistěte content-type
3. Implementujte Server/Klient s TCP komunikací.
    +  Obslužte více než jednoho klienta.
    +  Obslužte více klientů v jeden okamžik.
